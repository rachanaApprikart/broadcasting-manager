//
//  VCManager.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 21/06/24.
//

import Foundation
import UIKit

struct VCManager {
    
    static let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    
    static func openLoginnVC() -> LoginViewController? {
        return storyBoard.instantiateViewController(identifier: LoginViewController.sbIdentifier) as? LoginViewController
    }
    
    static func openConfigurationVC() -> ConfigurationVC? {
        return storyBoard.instantiateViewController(identifier: ConfigurationVC.sbIdentifier) as? ConfigurationVC
    }
    
    static func openOTPVC() -> GetOTPViewController? {
        
        guard let vc = storyBoard.instantiateViewController(identifier: GetOTPViewController.sbIdentifier) as? GetOTPViewController else { return nil }
        
        vc.modalTransitionStyle = .coverVertical
        vc.modalPresentationStyle = .overCurrentContext
        
        return vc
    }
    
}

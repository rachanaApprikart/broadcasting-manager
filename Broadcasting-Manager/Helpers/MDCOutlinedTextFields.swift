//
//  MDCOutlinedTextFields.swift
//  AppStreamer
//
//  Created by Rachana on 07/02/24.
//

import Foundation
import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields


extension MDCOutlinedTextField {
    
    func applyThemeForTextField(placeHolderText: String) {
        self.placeholder = placeHolderText
       
      //  self.label.font = UIFont(name: Fonts.montserratMedium, size: 12.0)
     //   self.setFloatingLabelColor(ColorCode.placeHolderLabelColor, for: .normal)
      //  self.font = UIFont(name: Fonts.montserratMedium, size: 14.0)
        // self.preferredContainerHeight = 45
        self.leadingEdgePaddingOverride = 15
        self.setOutlineColor(.lightGray, for: .normal)
        self.setOutlineColor(.darkGray, for: .editing)
        self.setTextColor(.black, for: .normal)

        
        self.label.text = placeHolderText
        
        self.backgroundColor = .white
        self.sizeToFit()
    }
    
    
}

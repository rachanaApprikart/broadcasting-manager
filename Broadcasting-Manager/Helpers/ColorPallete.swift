//
//  ColorPallete.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 20/06/24.
//

import Foundation
import UIKit


class ColorCode {
    
    static let hyundai_blue = UIColor(red: 0, green: 44, blue: 95, alpha: 1)
    static let primaryBlueColor = UIColor(red: 21/255.0, green: 129/255.0, blue: 198/255.0, alpha: 1.0)
    
    static let btnTitleColor = UIColor(red: 97/255.0, green: 124/255.0, blue: 214/255.0, alpha: 1.0)
    static let disabledBtnTitleColor = UIColor(red: 171/255.0, green: 171/255.0, blue: 171/255.0, alpha: 1.0)

    static let labelTextColor = UIColor(red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1.0)
    static let descpLabelTextColor = UIColor(red: 84/255.0, green: 84/255.0, blue: 84/255.0, alpha: 1.0)

}



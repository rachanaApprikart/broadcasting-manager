//
//  ToastViewController.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 24/06/24.
//

import Foundation
import ToastViewSwift

struct ToastViewHelper {
    static let shared = ToastViewHelper()
    
    
    func showToast(title: String, subTitle: String? = nil, direction: Toast.Direction = .bottom) {
        
        let toastConfig = ToastConfiguration(direction: direction)
        if let subTitleVal = subTitle {
            
            let titleAttributes = [
                NSAttributedString.Key.font: UIFont(name: Fonts.interBold, size: 16)!,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ]
            let titleStr  = NSMutableAttributedString(string: title , attributes: titleAttributes)
            
            let subTitleAttributes = [
                NSAttributedString.Key.font: UIFont(name: Fonts.interBold, size: 12)!,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ]
            let subTitleStr  = NSMutableAttributedString(string: subTitleVal , attributes: subTitleAttributes)
            
            let toast = Toast.text(titleStr, subtitle: subTitleStr, config: toastConfig)
            toast.show()
        } else {
            
            let titleAttributes = [
                NSAttributedString.Key.font: UIFont(name: Fonts.interBold, size: 14)!,
                NSAttributedString.Key.foregroundColor: UIColor.black
            ]
            let titleStr = NSMutableAttributedString(string: title , attributes: titleAttributes)
            let toast = Toast.text(titleStr, config: toastConfig)
            toast.show()
        }
       
    }
}

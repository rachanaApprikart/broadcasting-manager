//
//  AppUserDefauls.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation

struct AppUserDefaults {
    
    static let userDefaults = UserDefaults.standard
    
    
    static var dealerDetails: DealerDetails? {
        get {
            return self.userDefaults.retrieve(object: DealerDetails.self, fromKey: "MANAGER_LOGIN_KEY") ?? nil
            
        } set {
            self.userDefaults.save(customObject: newValue, inKey: "MANAGER_LOGIN_KEY")
            
        }
    }
    
    static var YOUTUBE_TOKENS_DATA: YoutubeTokenData? {
        get {
            return self.userDefaults.retrieve(object: YoutubeTokenData.self, fromKey: "YOUTUBE_TOKEN_KEY") ?? nil
        } set {
            self.userDefaults.save(customObject: newValue, inKey: "YOUTUBE_TOKEN_KEY")
        }
    }
    
}


extension UserDefaults {
    
    func save<T:Encodable>(customObject object: T, inKey key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            self.set(encoded, forKey: key)
        }
    }
    
    func retrieve<T:Decodable>(object type:T.Type, fromKey key: String) -> T? {
        if let data = self.data(forKey: key) {
            let decoder = JSONDecoder()
            if let object = try? decoder.decode(type, from: data) {
                return object
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
}


struct YoutubeTokenData: Codable {
    var refreshToken: String?
    var accessToken: String?
}

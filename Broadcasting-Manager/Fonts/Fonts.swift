//
//  Fonts.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 20/06/24.
//

import Foundation
import UIKit

struct Fonts {
    static let interMedium: String = "Inter-Medium"
    
    static let interRegular: String = "Inter-Regular"
    
    static let interExtraBold: String = "Inter-ExtraBold"
    static let interSemiBold: String = "Inter-SemiBold"
    static let interBold: String = "Inter-Bold"
  
    static let interExtraLight: String = "Inter-ExtraLight"
    static let interLight: String = "Inter-Light"
    
    static let interThin: String = "Inter-Thin"
    static let interBlack: String = "Inter-Black"
}

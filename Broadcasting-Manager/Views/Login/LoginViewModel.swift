//
//  LoginViewModel.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


class LoginViewModel: NSObject {
    
    let loginTitle: String = "Login"
    let userNameText: String = "Username"
    let passwordText: String = "Password"
    
    
    let service = ManagerLoginService()
    
    override init() {
        super.init()
        
    }
    
    func getLoginUsing(req: LoginRequest) async throws -> LoginResponse {
        return try await service.loginUsing(loginReq: req)
    }
}
 

//
//  ViewController.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 20/06/24.
//

import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailTextField: MDCOutlinedTextField!
    @IBOutlet weak var passwordTextField: MDCOutlinedTextField!
    
    @IBOutlet weak var signInHeaderLabel: UILabel!
    @IBOutlet weak var loginBtn: UIButton!
   
    static var sbIdentifier: String {
        return String(describing: LoginViewController.self)
    }
    
    let loginVM = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setLoginScreenUI()
        self.emailTextField.text = "Sunil"
        self.passwordTextField.text = "Apprikart"
    }
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        sender.isUserInteractionEnabled = false
        defer {
            sender.isUserInteractionEnabled = true
        }
        
        guard let userNameStr = self.emailTextField.text, (userNameStr.count > 0)  else {
            ToastViewHelper.shared.showToast(title: errorTitle, subTitle: pleaseEnterAllFieldsTitle)
            return
        }
        
        guard let passwordStr = self.passwordTextField.text, (passwordStr.count > 0)  else {
            ToastViewHelper.shared.showToast(title: errorTitle, subTitle: pleaseEnterAllFieldsTitle)
            return
        }
        
        guard Reachability.isConnectedToNetwork() else {
            ToastViewHelper.shared.showToast(title: internetErrorTitle)
            return
        }
        let loginReq = LoginRequest(userName: userNameStr, password: passwordStr)
        self.loginManager(request: loginReq)
       
    }
    
    private func loginManager(request: LoginRequest) {
        Task {
            do {
                self.showLoader()
                let loginResp = try await self.loginVM.getLoginUsing(req: request)
                
                DispatchQueue.main.async {
                    self.hideLoader()
                    
                    if let dealerDetails = loginResp.details, (loginResp.status == APIStatus.success) {
                        AppUserDefaults.dealerDetails = dealerDetails
                        self.navigateToConfigurationVC()
                    } else {
                        ToastViewHelper.shared.showToast(title: errorTitle, subTitle: loginResp.error ?? unknownErrorTitle)
                    }
                }
            } catch let error {
                self.hideLoader()
                ToastViewHelper.shared.showToast(title: errorTitle, subTitle: error.localizedDescription)
            }
        }
    }
    
    private func navigateToConfigurationVC() {
        guard let sceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate,
              let vc = VCManager.openConfigurationVC() else {
            return
        }
        let navVC = UINavigationController(rootViewController: vc)
        navVC.setNavigationBarHidden(true, animated: true)
        sceneDelegate.window?.rootViewController = navVC
    }
}

//MARK: ALL ABOUT UI
extension LoginViewController {
    
    private func setLoginScreenUI() {
        
        self.emailTextField.applyThemeForTextField(placeHolderText: self.loginVM.userNameText)
        self.passwordTextField.applyThemeForTextField(placeHolderText: self.loginVM.passwordText)
        
        self.signInHeaderLabel.text = self.loginVM.loginTitle
        self.signInHeaderLabel.textColor = ColorCode.labelTextColor
        self.signInHeaderLabel.textAlignment = .center
        self.signInHeaderLabel.numberOfLines = 1
        self.signInHeaderLabel.font = UIFont(name: Fonts.interBold, size: 22)
        
        self.loginBtn.layer.cornerRadius = 10
        self.loginBtn.setTitle(self.loginVM.loginTitle, for: .normal)
        self.loginBtn.setTitleColor(.white, for: .normal)
        self.loginBtn.backgroundColor = ColorCode.primaryBlueColor
        self.loginBtn.titleLabel?.font =  UIFont(name: Fonts.interMedium, size: 16)
    }
}



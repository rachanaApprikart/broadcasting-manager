//
//  EventListTVCell.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 23/06/24.
//

import UIKit
import Kingfisher

class EventListTVCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var carModelNameLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var eventTimeLabel: UILabel!
    
    @IBOutlet weak var getOTPBtn: UIButton!
    
    var getOTPAction: (()->())?

    
    static var reUseIdentifier: String {
        return String(describing: EventListTVCell.self)
    }
    
    static var nibFile: UINib {
        return UINib(nibName: EventListTVCell.reUseIdentifier, bundle: nil)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setEventListUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func getOtpBtnAction(_ sender: UIButton) {
        self.getOTPAction?()
    }
    
    
    func setUpEventDetails(eventData: EventData) {
              
        if let estartTime = eventData.eventStart, (estartTime.count > 0), let eEndTime = eventData.eventEnd, (eEndTime.count > 0),let eventDate = eventData.eventDate, (eventDate.count > 0) {
            self.eventTimeLabel.text =  estartTime + " - " + eEndTime
            self.eventDateLabel.text = eventDate
            
 
            if let imageUrl = URL(string: eventData.eventImageURL ?? "") {
               self.carImageView.kf.indicatorType = .activity
               self.carImageView.kf.setImage(with: imageUrl, placeholder: nil)
            }
        }
        
        self.getOTPBtn.isEnabled = !(eventData.goneLive ?? true)
    }
}


extension EventListTVCell {
    
    private func setEventListUI()
    {
        self.selectionStyle = .none
        
        self.mainView.layer.cornerRadius = 10
        
        self.carModelNameLabel.text = "CRETA"
        self.carModelNameLabel.textAlignment = .left
        self.carModelNameLabel.numberOfLines = 1
        self.carModelNameLabel.textColor = .black
        self.carModelNameLabel.font = UIFont(name: Fonts.interSemiBold, size: 14)
        
        self.eventDateLabel.text = "23-10-2023"
        self.eventDateLabel.textAlignment = .left
        self.eventDateLabel.numberOfLines = 1
        self.eventDateLabel.textColor = ColorCode.descpLabelTextColor
        self.eventDateLabel.font = UIFont(name: Fonts.interRegular, size: 14)
        
        self.eventTimeLabel.text = "20:14"
        self.eventTimeLabel.textAlignment = .left
        self.eventTimeLabel.numberOfLines = 1
        self.eventTimeLabel.textColor = ColorCode.descpLabelTextColor
        self.eventTimeLabel.font = UIFont(name: Fonts.interRegular, size: 14)
        
        self.getOTPBtn.setTitle("Get OTP", for: .normal)
        self.getOTPBtn.setTitle("Completed", for: .disabled)

        self.getOTPBtn.backgroundColor = ColorCode.btnTitleColor.withAlphaComponent(0.12)
        self.getOTPBtn.layer.cornerRadius = 20
        
        self.getOTPBtn.setTitleColor(ColorCode.btnTitleColor, for: .normal)
        self.getOTPBtn.setTitleColor(ColorCode.disabledBtnTitleColor, for: .disabled)

        self.getOTPBtn.titleLabel?.font = UIFont(name: Fonts.interSemiBold, size: 12)
        
        self.carImageView.image = nil
    }
}

//
//  ConfigurationVC.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 21/06/24.
//

import UIKit
import FBSDKLoginKit
import AppAuth

class ConfigurationVC: BaseViewController {
    
    @IBOutlet weak var configurationSegmentControll: UISegmentedControl!
    
    //Configuration view
    
    @IBOutlet weak var configurationView: UIView!
    @IBOutlet weak var configHeader: UILabel!
    
    @IBOutlet weak var youtubrImageView: UIImageView!
    @IBOutlet weak var youTubeLabel: UILabel!
    @IBOutlet weak var youTubrLoginBtn: UIButton!
    
    @IBOutlet weak var fbImageView: UIImageView!
    @IBOutlet weak var fbLabel: UILabel!
    @IBOutlet weak var fbLoginBtn: UIButton!
    //Event list view
    
    @IBOutlet weak var eventListView: UIView!
    @IBOutlet weak var eventListTV: UITableView!
    
    let configVM = ConfigurationViewModel()
    
    var authState: OIDAuthState?
    var logTextView: String = ""

    fileprivate var events =  [EventData]() {
        didSet {
            DispatchQueue.main.async {
                self.eventListTV.reloadData()
            }
        }
    }
    
    static var sbIdentifier: String {
        return String(describing: ConfigurationVC.self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setConfigScreenUI()
        
        if let token = AccessToken.current, !token.isExpired {
            let tokenString = token.tokenString
            self.saveTokenInServer(token: tokenString, platform: .facebook)
            self.fbLoginBtn.setTitle("Logout", for: .normal)

        } else {
            self.removeTokenFromServer(platformType: .facebook)
            self.fbLoginBtn.setTitle("Login", for: .normal)
        }
        
        self.getRefreshTokenFromApprikartServer(platformType: .youtube)
    }
    
    @IBAction func configSegmentAction(_ sender: UISegmentedControl) {
        guard Reachability.isConnectedToNetwork() else {
            ToastViewHelper.shared.showToast(title: internetErrorTitle)
            return
        }
        sender.selectedSegmentIndex == 0 ? self.showConfigurationView() : self.configureEventList()
    }
    
    @IBAction func youTubeLoginBtnAction(_ sender: UIButton) {
        if youTubrLoginBtn.tag == 0 {
           self.authenticationRequest()
        } else {
            self.revokeAccessToken()
        }
    }
    
    private func getAllEventList() {
        
        guard Reachability.isConnectedToNetwork() else {
            ToastViewHelper.shared.showToast(title: internetErrorTitle)
            return
        }
        guard let dealerCode = AppUserDefaults.dealerDetails?.dealerCode else { return }
        
        Task {
            do {
                self.showLoader(custom: "Load all events ...")
                let allEventsData = try await self.configVM.getEventListUsing(dealerCode: dealerCode)
                
                self.hideLoader()
                if let events = allEventsData.events, (events.count > 0) {
                    self.events = allEventsData.events ?? []
                } else {
                    ToastViewHelper.shared.showToast(title: errorTitle, subTitle: "No events")
                }
            } catch let error {
                self.hideLoader()
                ToastViewHelper.shared.showToast(title: errorTitle, subTitle: error.localizedDescription)
            }
        }
    }
}

//MARK: TABLE VIEW DELEGATES

extension ConfigurationVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.events.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let tvCell = tableView.dequeueReusableCell(withIdentifier: EventListTVCell.reUseIdentifier, for: indexPath) as? EventListTVCell else {
            return UITableViewCell()
        }
        
        let event = self.events[indexPath.row]
        tvCell.setUpEventDetails(eventData: event)
        
        tvCell.getOTPAction = {
            guard let vc = VCManager.openOTPVC() else { return }
            vc.otp = event.otp
            self.present(vc, animated: true)
        }
        return tvCell
    }
}

//MARK: UI

extension ConfigurationVC {
    
    private func setConfigScreenUI() {
        
        self.registerForTVCell()
        
        self.configurationSegmentControll.setTitle(self.configVM.configTitle, forSegmentAt: 0)
        self.configurationSegmentControll.setTitle(self.configVM.eventListTitle, forSegmentAt: 1)
        self.configurationSegmentControll.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont(name: Fonts.interRegular, size: 14)!], for: .normal)
        self.configurationSegmentControll.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ColorCode.btnTitleColor, NSAttributedString.Key.font: UIFont(name: Fonts.interExtraBold, size: 14)!], for: .selected)
        self.configurationSegmentControll.selectedSegmentIndex = 0
        
        
        self.youTubeLabel.text = "Youtube"
        self.youTubeLabel.textAlignment = .left
        self.youTubeLabel.numberOfLines = 1
        self.youTubeLabel.textColor = ColorCode.labelTextColor
        self.youTubeLabel.font = UIFont(name: Fonts.interRegular, size: 14)
        
        self.fbLabel.text = "Facebook"
        self.fbLabel.textAlignment = .left
        self.fbLabel.numberOfLines = 1
        self.fbLabel.textColor = ColorCode.labelTextColor
        self.fbLabel.font = UIFont(name: Fonts.interRegular, size: 14)
        
        self.youTubrLoginBtn.setTitle(self.configVM.loginTitle, for: .normal)
        self.youTubrLoginBtn.backgroundColor = ColorCode.btnTitleColor.withAlphaComponent(0.12)
        self.youTubrLoginBtn.layer.cornerRadius = 20
        self.youTubrLoginBtn.setTitleColor(ColorCode.btnTitleColor, for: .normal)
        self.youTubrLoginBtn.titleLabel?.font = UIFont(name: Fonts.interMedium, size: 14)
        
        self.fbLoginBtn.backgroundColor = ColorCode.btnTitleColor.withAlphaComponent(0.12)
        self.fbLoginBtn.layer.cornerRadius = 20
        self.fbLoginBtn.setTitleColor(ColorCode.btnTitleColor, for: .normal)
        self.fbLoginBtn.titleLabel?.font = UIFont(name: Fonts.interMedium, size: 14)
        self.fbLoginBtn.addTarget(self, action: #selector(customLoginButtonTapped), for: .touchUpInside)

        self.configHeader.text = self.configVM.configHeader
        self.configHeader.textAlignment = .left
        self.configHeader.numberOfLines = 0
        self.configHeader.textColor = ColorCode.labelTextColor
        self.configHeader.font = UIFont(name: Fonts.interBold, size: 18)
        
    }
    
    @objc func customLoginButtonTapped() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["pages_show_list","pages_read_engagement", "public_profile", "pages_manage_posts", "publish_video", "pages_read_user_content", "pages_manage_metadata", "pages_manage_engagement", "manage_pages", "instagram_basic"], from: self) { (result, error) in
            if let error = error {
                print("Login failed with error: \(error.localizedDescription)")
                return
            }
            
            guard let result = result, !result.isCancelled else {
                print("User cancelled login")
                return
            }
            self.fbLoginBtn.setTitle("Logout", for: .normal)
            self.saveTokenInServer(token: result.token?.tokenString ?? "", platform: .facebook)
        
        }
    }
    
    private func showConfigurationView() {
        self.eventListView.isHidden = true
        self.view.sendSubviewToBack(self.eventListView)
        
        self.configurationView.isHidden = false
        self.view.bringSubviewToFront(self.configurationView)
        
    }
    
    private func showEventListView() {
        self.configurationView.isHidden = true
        self.view.sendSubviewToBack(self.configurationView)
        
        self.eventListView.isHidden = false
        self.view.bringSubviewToFront(self.eventListView)
    }
    
    private func configureEventList() {
        self.showEventListView()
        self.getAllEventList()
    }
    
    
    fileprivate func registerForTVCell() {
        self.eventListTV.delegate = self
        self.eventListTV.dataSource = self
        self.eventListTV.register(EventListTVCell.nibFile, forCellReuseIdentifier: EventListTVCell.reUseIdentifier)
        self.eventListTV.separatorStyle = .none
        self.eventListTV.estimatedRowHeight = 150
        self.eventListTV.showsVerticalScrollIndicator = false
    }
    
     func showLogindetails() {
        self.youTubrLoginBtn.setTitle("Google Sign in", for: .normal)
        self.youTubrLoginBtn.tag = 0
    }
    
     func showLogOutDetails() {
        self.youTubrLoginBtn.setTitle("SignOut", for: .normal)
        self.youTubrLoginBtn.tag = 1
    }
}

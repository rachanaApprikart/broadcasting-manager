//
//  ConfigurationViewModel.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 24/06/24.
//

import Foundation


class ConfigurationViewModel: NSObject {
    
    let configHeader: String = "Configure using dealer social media account."
    let loginTitle: String = "Login"
    let configTitle: String = "Configuration"
    let eventListTitle: String = "Event List"
    
    
    private let platformService = PlatformTokenService()
    private let eventService = EventService()
    
    
    override init() {
        super.init()
    }
    
    
    func getPlatformTokenUsing(dealerCode: String, platformType: PlatformType) async throws -> PlatformTokenRespData {
        return try await platformService.getPlatformToken(dealerCode: dealerCode, platform: platformType)
    }
    func removePlatformTokenUsing(dealerCode: String, platformType: PlatformType) async throws -> TokenDeleteResponse {
        return try await platformService.deletePlatformToken(dealerCode: dealerCode, platform: platformType)
    }
    
    func savePlatformTokenUsing(requestBody: SavePlatformToken) async throws -> TokenDeleteResponse {
        return try await platformService.savePlatformToken(requestBody: requestBody)
    }
    
    func getEventListUsing(dealerCode: String) async throws -> AllEventDetails {
        return try await eventService.getEventListUsing(dealerCode: dealerCode)
    }
    
    func getAccessTokenUsingRefresh(requestBody: YAccessTokenRequest) async throws -> OAuth2TokenData {
        return try await platformService.getAccessTokenUsingRefresh(requestBody: requestBody)
    }
}

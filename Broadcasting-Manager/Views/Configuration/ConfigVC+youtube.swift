//
//  ConfigVC+youtube.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 26/06/24.
//

import Foundation
import AppAuth
import CommonCrypto


extension ConfigurationVC {
    
    func getRefreshTokenFromApprikartServer(platformType: PlatformType) {
        guard Reachability.isConnectedToNetwork() else {
            ToastViewHelper.shared.showToast(title: internetErrorTitle)
            return
        }
        
        guard let dealerCode = AppUserDefaults.dealerDetails?.dealerCode else {
            ToastViewHelper.shared.showToast(title: "No dealer found")
            return
        }
        
        Task {
            do {
                self.showLoader()
                
                let tokensData = try await self.configVM.getPlatformTokenUsing(dealerCode: dealerCode, platformType: platformType)
                
                if let refreshToken = tokensData.token {
                    let accessTokenReq = YAccessTokenRequest(clientId: WEB_CLIENT_KEY, grantType: "refresh_token", refreshToken: refreshToken)
                    
                    let yAccessTokenResp = try await self.configVM.getAccessTokenUsingRefresh(requestBody: accessTokenReq)
                    
                    
                    DispatchQueue.main.async {
                        self.hideLoader()
                        
                        if let accessToken = yAccessTokenResp.accessToken {
                            //Both refresh and access tokens are there
                            self.showLogOutDetails()
                            AppUserDefaults.YOUTUBE_TOKENS_DATA?.accessToken = accessToken
                            AppUserDefaults.YOUTUBE_TOKENS_DATA?.refreshToken = refreshToken
                        } else {
                            //Refresh token is present but no access token
                            self.removeTokenFromServer(platformType: .youtube)
                            self.showLogindetails()
                            AppUserDefaults.YOUTUBE_TOKENS_DATA = nil
                        }
                    }
                } else {
                    //No refresh or access tokens
                    self.showLogindetails()
                    AppUserDefaults.YOUTUBE_TOKENS_DATA = nil
                }
            } catch let error {
                self.showLogindetails()
                AppUserDefaults.YOUTUBE_TOKENS_DATA = nil
                ToastViewHelper.shared.showToast(title: errorTitle, subTitle: error.localizedDescription)
            }
        }
    }
    
    func authenticationRequest() {
        
        let issuer = URL(string: API.GOOGLE_AUTHENTICATION)!
        
        // discovers endpoints
        OIDAuthorizationService.discoverConfiguration(forIssuer: issuer) { configuration, error in
            guard configuration != nil else {
                print("Error retrieving discovery document: \(error?.localizedDescription ?? "Unknown error")")
                return
            }
            
            
            let authorizationEndpoint = URL(string: API.AUTHORISATION_ENDPOINT)!
            let tokenEndpoint = URL(string: "https://www.googleapis.com/oauth2/v4/token")!
            let configuration = OIDServiceConfiguration(authorizationEndpoint: authorizationEndpoint,
                                                        tokenEndpoint: tokenEndpoint)
            
            
            let redirectUrl = URL(string: REDIRECT_URL)!
            // builds authentication request
            let request = OIDAuthorizationRequest(configuration: configuration,
                                                  clientId: WEB_CLIENT_KEY,
                                                  clientSecret: WEB_SECRET_KEY,
                                                  scopes: [OIDScopeOpenID, "https://www.googleapis.com/auth/youtube.force-ssl"],
                                                  redirectURL: redirectUrl,
                                                  responseType: OIDResponseTypeCode,
                                                  additionalParameters: nil)
            
            // performs authentication request
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.currentAuthorizationFlow = OIDAuthState.authState(byPresenting: request, presenting: self) { authState, error in
                if let authState = authState {
                    self.setAuthState(authState)
                    print("Got authorization tokens. Access token: " +
                          "\(authState.lastTokenResponse?.accessToken ?? "nil")")
                    
                    print("Got refresh token . token: " +
                          "\(authState.lastTokenResponse?.refreshToken ?? "nil")")
                    if let accessToken = authState.lastTokenResponse?.accessToken, let refreshToken = authState.lastTokenResponse?.refreshToken {
                        
                        let tokensData = YoutubeTokenData(refreshToken: refreshToken, accessToken: accessToken)
                        AppUserDefaults.YOUTUBE_TOKENS_DATA = tokensData
                        self.saveTokenInServer(token: refreshToken, platform: .youtube)
                    } else if (authState.lastTokenResponse?.accessToken) != nil  {
                        ToastViewHelper.shared.showToast(title: "refresh token is not available...")
                        // self.verifyAccessToken()
                    } else {
                        ToastViewHelper.shared.showToast(title: "Access token is not available...")
                        
                    }
                } else {
                    self.setAuthState(nil)
                }
            }
        }
    }
    
    func setAuthState(_ authState: OIDAuthState?) {
        if (self.authState == authState) {
            return;
        }
        self.authState = authState;
        self.authState?.stateChangeDelegate = self;
        self.stateChanged()
    }
    
    func revokeAccessToken() {
        guard Reachability.isConnectedToNetwork() else {
            ToastViewHelper.shared.showToast(title: internetErrorTitle)
            return }
        
        guard let refreshToken = AppUserDefaults.YOUTUBE_TOKENS_DATA?.refreshToken else {
            AppUserDefaults.YOUTUBE_TOKENS_DATA = nil
            self.showLogindetails()
            return }
        
        let params = ["token": refreshToken]
        self.showLoader()
        
        Task {
            
            do{
                let response = try await OAuth2Manager.revokeAccessTokenUsingRefreshToken(postBody: params)
                self.hideLoader()
                if response == APIStatus.success.rawValue {
                    AppUserDefaults.YOUTUBE_TOKENS_DATA = nil
                    self.removeTokenFromServer(platformType: .youtube)
                }
            }
            catch {
                self.hideLoader()
                ToastViewHelper.shared.showToast(title: errorTitle, subTitle: error.localizedDescription)
            }
        }
    }
}


//MARK: OIDAuthState Delegate
extension ConfigurationVC: OIDAuthStateChangeDelegate, OIDAuthStateErrorDelegate {

    func didChange(_ state: OIDAuthState) {
        self.stateChanged()
    }

    func authState(_ state: OIDAuthState, didEncounterAuthorizationError error: Error) {
        self.logMessage("Received authorization error: \(error)")
    }
    
    func challenge(verifier: String) -> String {
        
        guard let verifierData = verifier.data(using: String.Encoding.utf8) else { return "error" }
            var buffer = [UInt8](repeating: 0, count:Int(CC_SHA256_DIGEST_LENGTH))
            verifierData.withUnsafeBytes {
                CC_SHA256($0.baseAddress, CC_LONG(verifierData.count), &buffer)
            }
        let hash = Data(_: buffer)
        print(hash)
        let challenge = hash.base64EncodedData()
        return String(decoding: challenge, as: UTF8.self)
            .replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
            .trimmingCharacters(in: .whitespaces)
    }
    
    
       func stateChanged() {
           self.saveState()
       }
       
       func logMessage(_ message: String?) {
           
           guard let message = message else {
               return
           }
           
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "hh:mm:ss";
           let dateString = dateFormatter.string(from: Date())
           
           // appends to output log
           DispatchQueue.main.async {
               let logText = "\(self.logTextView)\n\(dateString): \(message)"
           }
       }
       
       func saveState() {
           
           var data: Data? = nil
           
           if let authState = self.authState {
               data = NSKeyedArchiver.archivedData(withRootObject: authState)
           }
           
           if let userDefaults = UserDefaults(suiteName: "group.net.openid.appauth.Example") {
               userDefaults.set(data, forKey: kAppAuthExampleAuthStateKey)
               userDefaults.synchronize()
           }
       }
}

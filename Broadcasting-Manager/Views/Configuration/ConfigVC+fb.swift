//
//  ConfigVC+fb.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 26/06/24.
//

import Foundation
import FBSDKLoginKit

extension ConfigurationVC: LoginButtonDelegate {
    
    func loginButton(_ loginButton: FBSDKLoginKit.FBLoginButton, didCompleteWith result: FBSDKLoginKit.LoginManagerLoginResult?, error: Error?) {
       
        guard error == nil, let accessToken = result?.token else {
            return print(error ?? "Facebook access token is nil")
        }
        if let token = AccessToken.current,
           !token.isExpired {
            self.saveTokenInServer(token: accessToken.tokenString, platform: .facebook)
        }
//        loginButton.layoutIfNeeded()
        self.fbLoginBtn.setTitle("Logout", for: .normal)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginKit.FBLoginButton) {
        self.removeTokenFromServer(platformType: .facebook)
        self.fbLoginBtn.setTitle("Login", for: .normal)
    }
    
  //Save token in our server
    func saveTokenInServer(token: String, platform: PlatformType) {
        
        guard let dealerCode = AppUserDefaults.dealerDetails?.dealerCode else {
            ToastViewHelper.shared.showToast(title: "No dealer found")
            return
        }
        
        let saveTokenBody = SavePlatformToken(platformName: platform.rawValue, token: token, dealerCode: dealerCode)
        
        Task {
            do{
                self.showLoader()
                let response = try await self.configVM.savePlatformTokenUsing(requestBody: saveTokenBody)
                
                self.hideLoader()
                
                if response.status == APIStatus.success {
                    
                } else {
                    ToastViewHelper.shared.showToast(title: errorTitle, subTitle: response.status?.rawValue ?? "Unable to save token")
                }
            } catch {
                self.hideLoader()
                ToastViewHelper.shared.showToast(title: errorTitle, subTitle: error.localizedDescription)
            }
        }
    }
    
    //Remove token from our server
    
    func removeTokenFromServer(platformType: PlatformType) {
        
        guard let dealerCode = AppUserDefaults.dealerDetails?.dealerCode else {
            ToastViewHelper.shared.showToast(title: "No dealer found")
            return
        }
        
        Task{
            do {
                self.showLoader()
                let response = try await self.configVM.removePlatformTokenUsing(dealerCode: dealerCode, platformType: platformType)
                self.hideLoader()

                if response.status == APIStatus.success {
                    
                    DispatchQueue.main.async {
                        if platformType == .youtube {
                            self.showLogindetails()
                        }
                    }
                    
                } else {
                    ToastViewHelper.shared.showToast(title: errorTitle, subTitle: response.status?.rawValue ?? "Unable to remove token")
                }
            } catch {
                self.hideLoader()
                ToastViewHelper.shared.showToast(title: errorTitle, subTitle: error.localizedDescription)
            }
        }
    }
}

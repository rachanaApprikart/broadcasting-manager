//
//  GetOTPViewController.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 23/06/24.
//

import UIKit

class GetOTPViewController: UIViewController {
    
    @IBOutlet weak var dimmedView: UIView!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var otpLabel: UILabel!
    @IBOutlet weak var otpDescpLabel: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    
    var otp: String?
    
    static var sbIdentifier: String {
        return String(describing: GetOTPViewController.self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setOtpPopUpScreen()
    }
    
    @IBAction func okBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
}


extension GetOTPViewController {
    
    private func setOtpPopUpScreen() {
    
        self.view.backgroundColor = .clear
        
        self.view.backgroundColor = .clear
        self.dimmedView.backgroundColor = .black
        self.dimmedView.alpha = 0.8

        self.containerView.backgroundColor = .white
        self.containerView.layer.cornerRadius = 15
    
        self.otpLabel.numberOfLines = 1
        self.otpLabel.textColor = .black
        self.otpLabel.textAlignment = .center
        self.otpLabel.font = UIFont(name: Fonts.interBold, size: 32)
        self.otpLabel.text = self.otp

        self.otpDescpLabel.numberOfLines = 1
        self.otpDescpLabel.textColor = ColorCode.descpLabelTextColor
        self.otpDescpLabel.textAlignment = .center
        self.otpDescpLabel.font = UIFont(name: Fonts.interRegular, size: 14)
        self.otpDescpLabel.text = "Your OTP for live broadcasting"
        
        self.okBtn.layer.cornerRadius = 10
        self.okBtn.setTitle("OK", for: .normal)
        self.okBtn.setTitleColor(.white, for: .normal)
        self.okBtn.backgroundColor = ColorCode.primaryBlueColor
        self.okBtn.titleLabel?.font =  UIFont(name: Fonts.interMedium, size: 16)
    }
}

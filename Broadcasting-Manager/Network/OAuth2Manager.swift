//
//  OAuth2Manager.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 26/06/24.
//

import Foundation

class OAuth2Manager {
    
    /* REVOKE ACCESS TOKEN USING REFRESH TOKEN*/
    
    class func revokeAccessTokenUsingRefreshToken(postBody: [String: String]) async throws -> String {
        
        guard Reachability.isConnectedToNetwork()  else {
            throw APIErrorType.noInternet
        }
        
        let urlStr = API.OAUTH2_REVOKE_ACCESS_TOKEN
        
        guard let revokeTokenURL = URL(string: urlStr) else {
            throw APIErrorType.invalidURL
        }
        
        var request = URLRequest(url: revokeTokenURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonData = try JSONEncoder().encode(postBody)
        request.httpBody = jsonData
        
        do {
            let (data, urlResponse) = try await URLSession.shared.data(for: request)
            
            guard let httpResponse = urlResponse as? HTTPURLResponse, (httpResponse.statusCode == 200) else { throw APIErrorType.invalidResponse }
            return APIStatus.success.rawValue
          
        } catch let error {
            throw APIErrorType.customizedError(error.localizedDescription)
        }
    }
    
}


//
//  EventEndPoint.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


enum EventEndPoint {
    case getEventsUsing(dealerCode: String)

    private func getURLPath() -> String {
        switch self {
        case .getEventsUsing(dealerCode: let dealerCode):
            return "/api/get_event_list/?records_per_page=100&dealer_code=\(dealerCode)&current_page=1"
        }
    }

    func getFullPath() -> String {
        return API.APPRIKART_BASE_URL + self.getURLPath()
    }
}

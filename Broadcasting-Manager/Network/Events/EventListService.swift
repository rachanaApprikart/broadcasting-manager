//
//  EventListService.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


struct EventService {
    func getEventListUsing(dealerCode: String) async throws -> AllEventDetails {
        
        let fullPath = EventEndPoint.getEventsUsing(dealerCode: dealerCode).getFullPath()
        
        guard let url = URL(string: fullPath) else {
            throw APIErrorType.invalidURL
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        
        
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200 else {
            throw APIErrorType.invalidResponse
        }
        
        do {
            let allEvents = try JSONDecoder().decode(AllEventDetails.self, from: data)
            return allEvents
        } catch let error {
            throw APIErrorType.decodableReason(error.localizedDescription)
        }
    }
}

//
//  ManagerLoginEndpoint.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


enum ManagerLoginEndpoint {
    
    case loginUsing(username: String, password: String)
    
    private func getURLPath() -> String {
        switch self {
            case .loginUsing(username: let userNameVal, password: let passwordVal):
                return "/api/app_login/?user_name=\(userNameVal)&password=\(passwordVal)&user_type=manager"
        }
    }
    
    func getFullPath() -> String {
        return API.APPRIKART_BASE_URL + self.getURLPath()
    }
}

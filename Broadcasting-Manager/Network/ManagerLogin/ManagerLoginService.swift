//
//  ManagerLoginService.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


struct ManagerLoginService {
    
    func loginUsing(loginReq: LoginRequest) async throws -> LoginResponse {
        
        let fullPath = ManagerLoginEndpoint.loginUsing(username: loginReq.userName, password: loginReq.password).getFullPath()
        
        guard let url = URL(string: fullPath) else {
            throw APIErrorType.invalidURL
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        
        
        guard let httpResponse = response as? HTTPURLResponse,
              httpResponse.statusCode == 200 else {
            throw APIErrorType.invalidResponse
        }
        
        do {
            let sipData = try JSONDecoder().decode(LoginResponse.self, from: data)
            return sipData
        } catch let error {
            throw APIErrorType.decodableReason(error.localizedDescription)
        }
    }
}

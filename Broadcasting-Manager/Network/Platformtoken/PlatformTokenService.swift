//
//  PlatformTokenService.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 24/06/24.
//

import Foundation


struct PlatformTokenService {
    
    func savePlatformToken(requestBody: SavePlatformToken) async throws -> TokenDeleteResponse {
        
        guard Reachability.isConnectedToNetwork()  else {
            throw APIErrorType.noInternet
        }
        
        let urlStr = PlatformTokenEndpoint.saveToken.getFullPath()
        
        guard let saveTokenURL = URL(string: urlStr) else {
            throw APIErrorType.invalidURL
        }
        
        var request = URLRequest(url: saveTokenURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonData = try JSONEncoder().encode(requestBody)
        request.httpBody = jsonData
        
        do {
            let (data, urlResponse) = try await URLSession.shared.data(for: request)
            
            guard let httpResponse = urlResponse as? HTTPURLResponse, (httpResponse.statusCode == 200) else { throw APIErrorType.invalidResponse }
            
            do {
                let saveTokenResp = try JSONDecoder().decode(TokenDeleteResponse.self, from: data)
                return saveTokenResp
            } catch {
                throw APIErrorType.decodingError
            }
        } catch let error {
            throw APIErrorType.customizedError(error.localizedDescription)
        }
    }
    
    func getPlatformToken(dealerCode: String, platform: PlatformType) async throws -> PlatformTokenRespData {
        guard Reachability.isConnectedToNetwork()  else {
            throw APIErrorType.noInternet
        }
        
        let urlStr = PlatformTokenEndpoint.getToken(dealerCode: dealerCode, platform: platform).getFullPath()
        guard let productListURL = URL(string: urlStr) else {
            throw APIErrorType.invalidURL
        }
        
        do {
            let (data, urlResponse) = try await URLSession.shared.data(from: productListURL)
            
            guard let httpResponse = urlResponse as? HTTPURLResponse, (httpResponse.statusCode == 200) else { throw APIErrorType.invalidResponse }
            
            do {
                let respData = try JSONDecoder().decode(PlatformTokenRespData.self, from: data)
                return respData
            } catch {
                throw APIErrorType.decodingError
            }
        } catch let error {
            throw APIErrorType.customizedError(error.localizedDescription)
        }
    }
    
    
    func deletePlatformToken(dealerCode: String, platform: PlatformType) async throws -> TokenDeleteResponse {
        guard Reachability.isConnectedToNetwork()  else {
            throw APIErrorType.noInternet
        }
        
        let urlStr = PlatformTokenEndpoint.deleteToken(dealerCode: dealerCode, platform: platform).getFullPath()
        
        guard let tokenDeleteURL = URL(string: urlStr) else {
            throw APIErrorType.invalidURL
        }
        
        do {
            let (data, urlResponse) = try await URLSession.shared.data(from: tokenDeleteURL)
            
            guard let httpResponse = urlResponse as? HTTPURLResponse, (httpResponse.statusCode == 200) else { throw APIErrorType.invalidResponse }
            
            do {
                let deleteResp = try JSONDecoder().decode(TokenDeleteResponse.self, from: data)
                return deleteResp
            } catch {
                throw APIErrorType.decodingError
            }
        } catch let error {
            throw APIErrorType.customizedError(error.localizedDescription)
        }
    }
    
    //MARK: YOUTUBE
    
    func getAccessTokenUsingRefresh(requestBody: YAccessTokenRequest) async throws -> OAuth2TokenData {
        
        guard Reachability.isConnectedToNetwork()  else {
            throw APIErrorType.noInternet
        }
        
        let urlStr = PlatformTokenEndpoint.getAccessTokenUsingRefreshToken.getFullPathForGoogle()
        
        guard let getAccessTokenURL = URL(string: urlStr) else {
            throw APIErrorType.invalidURL
        }
        
        var request = URLRequest(url: getAccessTokenURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonData = try JSONEncoder().encode(requestBody)
        request.httpBody = jsonData
        
        do {
            let (data, urlResponse) = try await URLSession.shared.data(for: request)
            
            guard let httpResponse = urlResponse as? HTTPURLResponse, (httpResponse.statusCode == 200) else { throw APIErrorType.invalidResponse }
            
            do {
                let accessTokenResp = try JSONDecoder().decode(OAuth2TokenData.self, from: data)
                return accessTokenResp
            } catch {
                throw APIErrorType.decodingError
            }
        } catch let error {
            throw APIErrorType.customizedError(error.localizedDescription)
        }
    }
    
    /* REVOKE ACCESS TOKEN USING REFRESH TOKEN*/
    
    func revokeAccessTokenUsingRefreshToken(postBody: [String: String]) async throws -> Bool {
        
        guard Reachability.isConnectedToNetwork()  else {
            throw APIErrorType.noInternet
        }
        
        let urlStr = API.OAUTH2_REVOKE_ACCESS_TOKEN
        
        guard let revokeTokenURL = URL(string: urlStr) else {
            throw APIErrorType.invalidURL
        }
        
        var request = URLRequest(url: revokeTokenURL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonData = try JSONEncoder().encode(postBody)
        request.httpBody = jsonData
        
        do {
            let (data, urlResponse) = try await URLSession.shared.data(for: request)
            
            guard let httpResponse = urlResponse as? HTTPURLResponse, (httpResponse.statusCode == 200) else { throw APIErrorType.invalidResponse }
            return true
          
        } catch let error {
            throw APIErrorType.customizedError(error.localizedDescription)
        }
    }
}

//
//  PlatformTokenEndpoint.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 24/06/24.
//

import Foundation


enum PlatformTokenEndpoint {
    
    case saveToken
    case getToken(dealerCode: String, platform: PlatformType)
    case deleteToken(dealerCode: String, platform: PlatformType)
    case getAccessTokenUsingRefreshToken
    
    private func getURLPath() -> String {
        switch self {
            case .saveToken:
                return "/api/save_token/"
                
            case .getToken(dealerCode: let dealerCode, platform: let platformType):
                return "/api/get_token/?dealer_code=\(dealerCode)&platform_name=\(platformType.rawValue)"
                
            case .deleteToken(dealerCode: let dealerCode, platform: let platformType):
                return "/api/archive_token/?dealer_code=\(dealerCode)&platform_name=\(platformType.rawValue)"
                
            case .getAccessTokenUsingRefreshToken:
                return "https://oauth2.googleapis.com/token"
        }
    }
    
    
    func getFullPath() -> String {
        return API.APPRIKART_BASE_URL + self.getURLPath()
    }
    
    func getFullPathForGoogle() -> String {
        return self.getURLPath()
    }
}


enum PlatformType: String {
    case youtube = "Youtube"
    case facebook = "Facebook"
    case website = "Website"
}

//
//  LoginModels.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


struct LoginRequest: Codable {
    let userName: String
    let password: String
}

struct LoginResponse: Codable {
    let status: APIStatus
    let error: String?
    let details: DealerDetails?
    
    enum CodingKeys: String, CodingKey {
        case status
        case error = "err"
        case details = "details"
    }
}

struct DealerDetails: Codable {
    var dealerCode: String
    let userType: String
    
    enum CodingKeys: String, CodingKey {
        case dealerCode = "dealer_code"
        case userType = "user_type"
        
    }
}

enum UserType: String, Codable {
    case presenter
    case manager
}

enum APIStatus: String, Codable {
    case success
    case failed
}

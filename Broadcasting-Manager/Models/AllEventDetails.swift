//
//  AllEventDetails.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 25/06/24.
//

import Foundation


struct AllEventDetails: Codable {
    let meta: Meta?
    let events: [EventData]?
    
    enum CodingKeys: String, CodingKey {
        case meta = "meta"
        case events = "data"
    }
}

// MARK: - Datum
struct EventData: Codable {
    let eventID: Int?
    let eventName: String
    let eventDescription: String
    let eventImageURL: String?
    let eventDate, eventEnd, eventStart: String?
    var facebookId: String?
    var youtubeId: String?
    let goneLive: Bool?
    var isWebsiteSelected: Bool = false
    let channels: String?
    let carName: String?
    let otp: String?
    
    enum CodingKeys: String, CodingKey {
        case eventDescription = "event_description"
        case eventEnd = "event_end"
        case eventID = "event_id"
        case eventStart = "event_start"
        case eventName = "event_name"
        case eventImageURL = "event_image"
        case eventDate = "event_date"
        case facebookId = "scheduled_fb_id"
        case youtubeId = "scheduled_youtube_id"
        case goneLive = "gone_live"
        case channels = "channels"
        case carName = "car_name"
        case otp = "OTP"
    }
}

// MARK: - Meta
struct Meta: Codable {
    let totalRecords, recordLimitPerPage, currentPage, totalPages: Int

    enum CodingKeys: String, CodingKey {
        case totalRecords = "total_records"
        case recordLimitPerPage = "record_limit_per_page"
        case currentPage = "current_page"
        case totalPages = "total_pages"
    }
}

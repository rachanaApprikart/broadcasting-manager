//
//  PlatformTokenRespData.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 24/06/24.
//

import Foundation

//Response while getting the token

struct PlatformTokenRespData: Codable {
    let platformName:  String?
    let token: String?
    let dealerCode: String?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case platformName = "platform_name"
        case dealerCode = "dealer_code"
    }
}

//Response ehile saving/deleting the token
struct TokenDeleteResponse: Codable {
    let status: APIStatus?
}


//To save the token
struct SavePlatformToken: Codable {
    let platformName:  String?
    let token: String?
    let dealerCode: String?
    
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case platformName = "platform_name"
        case dealerCode = "dealer_code"
    }
}

//MARK: youtube

//Request access token
struct YAccessTokenRequest: Codable {
    let clientId: String
    let grantType: String
    let refreshToken: String
    
    enum CodingKeys: String, CodingKey {
        case clientId = "client_id"
        case grantType = "grant_type"
        case refreshToken = "refresh_token"
    }
}

//Response access token

struct OAuth2TokenData: Codable {
    let tokenType: String?
    let accessToken: String?
    let expiresIn: Int?
    let scope: String?
    
    enum CodingKeys: String, CodingKey {
        case tokenType = "token_type"
        case accessToken = "access_token"
        case expiresIn = "expires_in"
        case scope = "scope"
    }
}

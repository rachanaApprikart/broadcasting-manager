//
//  APIError.swift
//  Broadcasting-Manager
//
//  Created by Rachana on 24/06/24.
//

import Foundation

enum APIErrorType: Error {
    case invalidURL
    case invalidResponse
    case decodableFailed
    case decodableReason(String)
    case unknownReason
    
    case decodingError
    case noInternet
    case customizedError(String)
    
    
    var suggestedMessage: String {
        switch self {
        case .invalidURL:
            return "Invalid URL"
        case .invalidResponse:
            return "Invalid Response"
        case .decodableFailed:
            return "Decodable failed"
        case .decodableReason(let reason):
            return "Decodable failed \(reason)"
        case .unknownReason:
            return unknownErrorTitle
        case .decodingError:
            return "Decoding Error"
        case .noInternet:
            return internetErrorTitle
        case .customizedError(let errorStr):
            return errorStr
            
            
        }
    }
}



public enum NetworkError: Error, LocalizedError {
    
    case missingRequiredFields(String)
    
    case invalidParameters(operation: String, parameters: [Any])
    
    case badRequest
    
    case unAuthorized
    
    case paymentRequired
    
    case forbidden
    
    case notFound
    
    case requestEntityTooLarge

    case unProcessableEntity
    
    case http(httpResponse: HTTPURLResponse, data: Data)
    
    case invalidResponse(Data)
    
    case deleteOperationFailed(String)
    
    case network(URLError)
    
    case unknown(Error?)

}

func mapResponse(response: (data: Data, response: URLResponse)) throws -> Data {
    guard let httpResponse = response.response as? HTTPURLResponse else {
        return response.data
    }
    
    switch httpResponse.statusCode {
    case 200..<300:
        return response.data
    case 400:
        throw NetworkError.badRequest
    case 401:
        throw NetworkError.unAuthorized
    case 402:
        throw NetworkError.paymentRequired
    case 403:
        throw NetworkError.forbidden
    case 404:
        throw NetworkError.notFound
    case 413:
        throw NetworkError.requestEntityTooLarge
    case 422:
        throw NetworkError.unProcessableEntity
    default:
        throw NetworkError.http(httpResponse: httpResponse, data: response.data)
    }
}
